package com.saturncube.gadandco.appconfig;

/**
 * Created by pc4 on 5/12/2016.
 */
public class Application extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        //FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/DroidSans.ttf");
        FontsOverride.setDefaultFont(this, "MONOSPACE", "fonts/Roboto-Regular.ttf");
    }
}
