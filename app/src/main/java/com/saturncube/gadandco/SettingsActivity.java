package com.saturncube.gadandco;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saturncube.gadandco.api.ChangePasswordApi;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.DialogManager;
import com.saturncube.gadandco.common.Preferences;

import java.io.UnsupportedEncodingException;

public class SettingsActivity extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    ImageView img_back;
    TextView tv_header_title;

    RelativeLayout rel_change_password, rel_change_language;
    TextView tv_change_password, tv_change_language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        initView();


        rel_change_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changePasswordDialog(SettingsActivity.this);
            }
        });

        rel_change_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preferences.setValue(SettingsActivity.this, Preferences.USER_ID, "");
                Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    private void changePasswordDialog(final Context mContext) {
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mContext);
        final LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.dialog_change_password, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.show();

        final TextView tv_dialog_title = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
        final EditText edt_old_pass = (EditText) dialogView.findViewById(R.id.edt_old_pass);
        final EditText edt_new_pass = (EditText) dialogView.findViewById(R.id.edt_new_pass);
        final EditText edt_confirm_pass = (EditText) dialogView.findViewById(R.id.edt_confirm_pass);

        final TextView tv_save = (TextView) dialogView.findViewById(R.id.tv_save);
        final TextView tv_cancel = (TextView) dialogView.findViewById(R.id.tv_cancel);


        tv_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                boolean flagValidate = true;
                String validateMsg = "";
                if (edt_old_pass.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter old password";
                    flagValidate = false;
                } else if (edt_new_pass.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter new password";
                    flagValidate = false;
                } else if (edt_confirm_pass.getText().toString().trim().equals("")) {
                    validateMsg = "Please enter confirm password";
                    flagValidate = false;
                } else if (!edt_new_pass.getText().toString().equals(edt_confirm_pass.getText().toString())) {
                    validateMsg = "Not matched confirmed password";
                    flagValidate = false;
                } else {
                    flagValidate = true;
                }

                if (flagValidate) {
                    b.dismiss();
                    if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), SettingsActivity.this)) {
                        String envelope = "&user_id=" + Preferences.getValue_String(SettingsActivity.this, Preferences.USER_ID);
                        envelope = envelope + "&oldPass=" + edt_old_pass.getText().toString();
                        envelope = envelope + "&newPass=" + edt_new_pass.getText().toString();
                        new ChangePasswordApi(envelope, mContext, new ChangePasswordApi.OnResultReceived() {
                            @Override
                            public void onResult(String result) throws UnsupportedEncodingException {
                                Preferences.setValue(SettingsActivity.this, Preferences.USER_ID, "");
                                Intent intent = new Intent(mContext, LoginActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        }, true);
                    }
                } else {
                    DialogManager.errorDialog(mContext, "Error!", validateMsg);
                }


            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.dismiss();
            }
        });
    }


    private void initView() {
        img_back = (ImageView) findViewById(R.id.img_back);
        img_back.setVisibility(View.VISIBLE);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setText("Setting");
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        rel_change_password = (RelativeLayout) findViewById(R.id.rel_change_password);
        tv_change_password = (TextView) findViewById(R.id.tv_change_password);
        rel_change_language = (RelativeLayout) findViewById(R.id.rel_change_language);
        tv_change_language = (TextView) findViewById(R.id.tv_change_language);


    }
}
