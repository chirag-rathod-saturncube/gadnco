package com.saturncube.gadandco.common;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class WebServices {
    public static final String RESPONSE_UNWANTED = "UNWANTED";

    //private static final String BASE_URL = "http://demo.saturncube.com/gad&co/api.php?";
    private static final String BASE_URL = "http://gadcoinventoryonline.com/api/api.php?";

    public static final String LOGIN_URL = BASE_URL + "action=login";
    public static final String FORGOT_PASSWORD = BASE_URL + "action=confirmForgetPass";
    public static final String GET_CATEGORY = BASE_URL + "action=viewCategory";
    public static final String GET_FILTERS = BASE_URL + "action=viewdropdownlist";
    public static final String GET_ITEM = BASE_URL + "action=viewitemlist";
    public static final String GET_ITEM_DETAIL = BASE_URL + "action=viewitemdetaillist";
    public static final String CHANGE_PASSWORD = BASE_URL + "action=changePassword";



    public static String callGetRequest(Context mContext, String url) throws IOException {
        StringBuilder response = null;
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.addRequestProperty("Cache-Control", "no-cache");
        con.addRequestProperty("Content-Type", "application/json");

        //Retrieving Data
        BufferedReader bufferResponse;
        if (con.getResponseCode() / 100 == 2) {
            bufferResponse = new BufferedReader(new InputStreamReader(con.getInputStream()));
        } else {
            bufferResponse = new BufferedReader(new InputStreamReader(con.getErrorStream()));
        }

        String line;
        String newResponse = "";
        while ((line = bufferResponse.readLine()) != null) {
            newResponse = newResponse + line;
        }

        bufferResponse.close();
        return newResponse;
    }

    public static String uploadMultipartData(Context context, String server_url) {
        MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
        File file = new File("image_path");
        multipartEntity.addBinaryBody("image", file, ContentType.create("image/jpeg"), AppConstants.APP_NAME + "_" + file.getName());
        return multiPost(context, server_url, multipartEntity);
    }

    public static String uploadUserPic(Context context, String server_url) {
        MultipartEntityBuilder multipartEntity = MultipartEntityBuilder.create();
        if (!AppConstants.profile_path.equals("")) {
            Log.e(context.getClass().getSimpleName(), "Multipart image path: " + AppConstants.profile_path);
            File file = new File(AppConstants.profile_path);
            multipartEntity.addBinaryBody("profile_img", file, ContentType.create("image/jpeg"), "Sfindit_" + file.getName());
        }
        return multiPost(context, server_url, multipartEntity);
    }

    public static String multiPost(Context context, String urlString, MultipartEntityBuilder entityBuilder) {

        try {
            HttpEntity entity = entityBuilder.build();
            HttpURLConnection conn = (HttpURLConnection) new URL(urlString).openConnection();
            conn.setUseCaches(false);
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Accept-Charset", "UTF-8");
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Cache-Control", "no-cache");

            conn.setRequestProperty("Content-length", entity.getContentLength() + "");
            conn.setRequestProperty(entity.getContentType().getName(), entity.getContentType().getValue());
            OutputStream os = conn.getOutputStream();
            entity.writeTo(os);
            os.close();

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return readStream(conn.getInputStream());
            }

        } catch (Exception e) {
            Log.e("MainActivity", "multipart post error " + e + "(" + urlString + ")");
        }
        return null;
    }

    private static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return builder.toString();
    }

}
