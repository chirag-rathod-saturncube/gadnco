package com.saturncube.gadandco.common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.saturncube.gadandco.R;
import com.saturncube.gadandco.moduls.Category;
import com.saturncube.gadandco.moduls.Demo;
import com.saturncube.gadandco.moduls.Filter;
import com.saturncube.gadandco.moduls.Image;
import com.saturncube.gadandco.moduls.Item;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by pc4 on 2/24/2017.
 */

public class AppConstants {
    public static final String APP_CONSTANT = "APP_CONSTANT";



    public static final String APP_NAME = "GAD&Co";



    public static ArrayList<Demo> demoArrayList= new ArrayList<>();
    public static ArrayList<Category> categoryList= new ArrayList<>();
    public static ArrayList<Item> itemList= new ArrayList<>();
    public static ArrayList<Filter> cutList= new ArrayList<>();
    public static ArrayList<Filter> colorList= new ArrayList<>();
    public static ArrayList<Filter> caratList= new ArrayList<>();
    public static ArrayList<Filter> clarityList= new ArrayList<>();
    public static ArrayList<Image> imageList= new ArrayList<>();



    public static String stone="";
    public static String item_id="";
    public static String certificate="";
    public static String certificate_no="";
    public static String item_type="";
    public static String treatment="";
    public static String color="";
    public static String location="";
    public static String cut="";
    public static String clarity="";
    public static String carat="";
    public static String rapa_port_price="";
    public static String retail_price="";
    public static String wholesale_price="";
    public static String Category_name="";
    public static String Category_id="";
    public static String item_name="";
    public static String item_image="";


    public static String encodeType = "UTF-8";
    public static String profile_path = "";
    public static File mFileTemp;
    public static final String TEMP_PHOTO_FILE_NAME = "my_dp.jpg";


    public static boolean isNetworkAvailable(final String title, final String message, final Context context) {
        Config.getmycontext(context);
        if (Config.oConnectivityManager.getActiveNetworkInfo() != null && Config.oConnectivityManager.getActiveNetworkInfo().isAvailable() && Config.oConnectivityManager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            final View dialogView = inflater.inflate(R.layout.dialog_internet_connection, null);
            dialogBuilder.setView(dialogView);
            final AlertDialog b = dialogBuilder.create();

            b.show();


            final ImageView imgClose = (ImageView) dialogView.findViewById(R.id.img_close);
            final TextView tvTitle = (TextView) dialogView.findViewById(R.id.tv_dialog_title);
            final TextView tvMsg = (TextView) dialogView.findViewById(R.id.tv_dialog_msg);
            final TextView tvOk = (TextView) dialogView.findViewById(R.id.tv_ok);

            tvTitle.setText("Retry");
            tvOk.setText("OK");
            tvMsg.setText("Please check internet connectivity");

            imgClose.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();
                }
            });

            tvOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    b.dismiss();

                }
            });


            tvOk.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {

                    if (event.getAction() == MotionEvent.ACTION_DOWN) {
                        tvOk.setTextColor(Color.WHITE);
                    } else if (event.getAction() == MotionEvent.ACTION_UP) {
                        tvOk.setTextColor(Color.rgb(6, 190, 188));
                    }
                    return false;
                }
            });

            return false;
        }
    }

    public static boolean validateEmail(String email) {
        Pattern pattern = Pattern
                .compile("[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}");
        // Pattern pattern = Patterns.EMAIL_ADDRESS;
        return pattern.matcher(email).matches();
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }

    public static Bitmap getRescaledBitmap(String filePath) {
        Bitmap bitmap = null;
        try {
            int targetWidth = 480; // your arbitrary fixed limit
            int targetHeight = 480;
            File image = new File(filePath);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), options);
            int ImageWidth = bitmap.getWidth();
            int ImageHeight = bitmap.getHeight();
            Log.e("before WxH", ImageWidth + "x" + ImageHeight);
            if (ImageHeight >= ImageWidth) {
                // targetWidth = 480;
                targetHeight = (int) (ImageHeight * targetWidth / (double) ImageWidth); // casts

                if (ImageWidth > targetWidth) {
                    bitmap = ScalingUtilities.decodeFile(image.getAbsolutePath(), targetWidth, targetHeight);
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap,
                            targetWidth, targetHeight);
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, imagefile);
                } else {
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imagefile);
                }

                Log.e("after WxH", bitmap.getWidth() + "x" + bitmap.getHeight());

            } else {

                targetWidth = (int) (ImageWidth * targetHeight / (double) ImageHeight); // casts

                if (ImageHeight > targetHeight) {
                    bitmap = ScalingUtilities.decodeFile(
                            image.getAbsolutePath(), targetWidth, targetHeight);
                    bitmap = ScalingUtilities.createScaledBitmap(bitmap,
                            targetWidth, targetHeight);
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 80, imagefile);
                } else {
                    OutputStream imagefile = new FileOutputStream(
                            image.getAbsoluteFile());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, imagefile);
                }
                Log.e("WxH", bitmap.getWidth() + "x" + bitmap.getHeight());

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return bitmap;

    }

    public static Transformation getTransformation(final ImageView imageView) {
        final Transformation transformation = new Transformation() {
            @Override
            public Bitmap transform(Bitmap source) {
                int targetWidth = imageView.getWidth();
                if (targetWidth == 0) {
                    targetWidth = 140;
                }
                double aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                int targetHeight = (int) (targetWidth * aspectRatio);
                Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                if (result != source) {
                    // Same bitmap is returned if sizes are the same
                    source.recycle();
                }
                return result;
            }

            @Override
            public String key() {
                return "transformation" + " desiredWidth";
            }
        };
        return transformation;
    }

}
