package com.saturncube.gadandco.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * Created by Saturncube-5 on 6/6/2017.
 */

public class Download {
    String strURL;
    int pos;
    Bitmap bitmap = null;
    Context mContext;

    // pass image url and Pos for example i:
    public Download(String url, int position) {
        this.strURL = url;
        this.pos = position;
    }

    public Bitmap getBitmapImage() {
        downloadBitmapImage();
        return readBitmapImage();
    }

    void downloadBitmapImage() {
        InputStream input;
        try {
            URL url = new URL(strURL);
            input = url.openStream();
            byte[] buffer = new byte[1500];
            OutputStream output = new FileOutputStream("/sdcard/" + pos + ".jpg");
            try {
                int bytesRead = 0;
                while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
                    output.write(buffer, 0, bytesRead);
                }
            } finally {
                output.close();
                buffer = null;
            }
        } catch (Exception e) {
            Toast.makeText(mContext, e.toString(), Toast.LENGTH_LONG).show();
        }
    }

    Bitmap readBitmapImage() {
        String imageInSD = "/sdcard/mac/" + strURL;
        BitmapFactory.Options bOptions = new BitmapFactory.Options();
        bOptions.inTempStorage = new byte[16 * 1024];

        bitmap = BitmapFactory.decodeFile(imageInSD, bOptions);

        return bitmap;
    }

}
