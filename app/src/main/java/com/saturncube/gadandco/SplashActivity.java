package com.saturncube.gadandco;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.saturncube.gadandco.common.Preferences;


public class SplashActivity extends AppCompatActivity {
    private final int TIMEOUT = 2000;
    String TAG = getClass().getSimpleName();
    private static String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                onLogin();
            }
        }, TIMEOUT);
    }

    private void onLogin() {
        user_id = Preferences.getValue_String(getBaseContext(), Preferences.USER_ID);
        if (user_id != null) {
            if (!user_id.equals("")) {
                Intent Intent_login = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(Intent_login);
                finish();
            } else {
                Intent Intent_login = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(Intent_login);
                finish();
            }
        } else {
            Intent Intent_login = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(Intent_login);
            finish();
        }
    }
}
