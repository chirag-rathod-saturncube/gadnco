package com.saturncube.gadandco.moduls;

/**
 * Created by Saturncube10 on 04-Apr-17.
 */

public class Filter {

    String id,name,code;

    public Filter() {
    }

    public Filter(String id, String name, String code) {
        this.id = id;
        this.name = name;
        this.code = code;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String toString() {
        return this.name;
    }
}
