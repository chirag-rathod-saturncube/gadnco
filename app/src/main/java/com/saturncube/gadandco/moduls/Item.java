package com.saturncube.gadandco.moduls;

/**
 * Created by Saturncube10 on 04-May-17.
 */

public class Item {

    String item_id,stone,certificate,certificate_no,category_id,item_img,item_img_small,item_img_medium,item_img_large;

    public String getItem_img_small() {
        return item_img_small;
    }

    public void setItem_img_small(String item_img_small) {
        this.item_img_small = item_img_small;
    }

    public String getItem_img_medium() {
        return item_img_medium;
    }

    public void setItem_img_medium(String item_img_medium) {
        this.item_img_medium = item_img_medium;
    }

    public String getItem_img_large() {
        return item_img_large;
    }

    public void setItem_img_large(String item_img_large) {
        this.item_img_large = item_img_large;
    }

    public String getItem_img() {
        return item_img;
    }

    public void setItem_img(String item_img) {
        this.item_img = item_img;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getStone() {
        return stone;
    }

    public void setStone(String stone) {
        this.stone = stone;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getCertificate_no() {
        return certificate_no;
    }

    public void setCertificate_no(String certificate_no) {
        this.certificate_no = certificate_no;
    }
}
