package com.saturncube.gadandco.moduls;

/**
 * Created by Saturncube-5 on 5/17/2017.
 */

public class Image {
    String image_id,item_id,item_pic,item_img_small,item_img_medium,item_img_large;

    public String getItem_id() {
        return item_id;
    }

    public void setItem_id(String item_id) {
        this.item_id = item_id;
    }

    public String getItem_pic() {
        return item_pic;
    }

    public void setItem_pic(String item_pic) {
        this.item_pic = item_pic;
    }

    public String getItem_img_small() {
        return item_img_small;
    }

    public void setItem_img_small(String item_img_small) {
        this.item_img_small = item_img_small;
    }

    public String getItem_img_medium() {
        return item_img_medium;
    }

    public void setItem_img_medium(String item_img_medium) {
        this.item_img_medium = item_img_medium;
    }

    public String getItem_img_large() {
        return item_img_large;
    }

    public void setItem_img_large(String item_img_large) {
        this.item_img_large = item_img_large;
    }

    public String getImage_id() {
        return image_id;
    }

    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getItem_image() {
        return item_pic;
    }

    public void setItem_image(String item_image) {
        this.item_pic = item_image;
    }
}
