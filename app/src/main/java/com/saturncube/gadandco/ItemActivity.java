package com.saturncube.gadandco;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saturncube.gadandco.adapter.ItemAdapter;
import com.saturncube.gadandco.api.GetItemApi;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.Preferences;
import com.saturncube.gadandco.moduls.Item;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Locale;

public class ItemActivity extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    ImageView img_back, img_search;
    TextView tv_header_title;
    EditText edt_searchbar;



    RelativeLayout rel_no_record,rel_searchbar;
    RecyclerView recycler_view;
    ItemAdapter adapter;
    LinearLayoutManager mLayoutManager;

    ImageView img_title, img_option;
    public static ArrayList<Item> itemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item);
        initView();


        LoadData();


        img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        img_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ItemActivity.this, Filter_Activity.class));
            }
        });

        edt_searchbar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = edt_searchbar.getText().toString().toLowerCase(Locale.getDefault());
                if (text.length() == 0) {
                    LoadData();
                } else {
                    if (adapter != null) {
                        adapter.filter(text);
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }

    private void initView() {
        img_back = (ImageView) findViewById(R.id.img_back);
        img_search = (ImageView) findViewById(R.id.img_search);
        edt_searchbar = (EditText) findViewById(R.id.edt_searchbar);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_header_title.setText(AppConstants.Category_name.toUpperCase());
        tv_header_title.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rel_no_record = (RelativeLayout) findViewById(R.id.rel_no_record);
        rel_searchbar = (RelativeLayout) findViewById(R.id.rel_searchbar);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new LinearLayoutManager(ItemActivity.this);
        boolean includeEdge = true;
        int spacing = 2;
        //recycler_view.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));

        mLayoutManager.setSmoothScrollbarEnabled(true);
        recycler_view.setLayoutManager(mLayoutManager);


        img_title = (ImageView) findViewById(R.id.img_title);
        img_title.setVisibility(View.GONE);
        img_option = (ImageView) findViewById(R.id.img_option);
        img_option.setImageResource(R.drawable.ic_filter);
        img_option.setVisibility(View.VISIBLE);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (adapter != null) {
            if (AppConstants.itemList.size() > 0) {
                rel_no_record.setVisibility(View.GONE);
                recycler_view.setVisibility(View.VISIBLE);
                rel_searchbar.setVisibility(View.VISIBLE);
                adapter = new ItemAdapter(ItemActivity.this, AppConstants.itemList);
                recycler_view.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            } else {
                recycler_view.setVisibility(View.GONE);
                rel_searchbar.setVisibility(View.GONE);
                rel_no_record.setVisibility(View.VISIBLE);
            }

        }
    }

    public void LoadData() {
        if (AppConstants.isNetworkAvailable(ItemActivity.this.getString(R.string.network_title), getString(R.string.network_msg), ItemActivity.this)) {
            String param = "&cat_id=" + AppConstants.Category_id;
            param = param + "&role_id=" + Preferences.getValue_String(ItemActivity.this, Preferences.USER_ROLE);
            param = param + "&cut=" + Preferences.getValue_String(ItemActivity.this, Preferences.CUT);
            param = param + "&color=" + Preferences.getValue_String(ItemActivity.this, Preferences.COLOR);
            param = param + "&clarity=" + Preferences.getValue_String(ItemActivity.this, Preferences.CLARITY);
            param = param + "&carat=" + Preferences.getValue_String(ItemActivity.this, Preferences.CARAT);
            param = param + "&startprice=" + Preferences.getValue_String(ItemActivity.this, Preferences.START_PRICE);
            param = param + "&endprice=" + Preferences.getValue_String(ItemActivity.this, Preferences.END_PRICE);
            new GetItemApi(param, ItemActivity.this, new GetItemApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    if (AppConstants.itemList.size() > 0) {
                        rel_no_record.setVisibility(View.GONE);
                        recycler_view.setVisibility(View.VISIBLE);
                        rel_searchbar.setVisibility(View.VISIBLE);
                        adapter = new ItemAdapter(ItemActivity.this, AppConstants.itemList);
                        recycler_view.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        recycler_view.setVisibility(View.GONE);
                        rel_searchbar.setVisibility(View.GONE);
                        rel_no_record.setVisibility(View.VISIBLE);
                    }
                }
            }, true);

        }


    }
}
