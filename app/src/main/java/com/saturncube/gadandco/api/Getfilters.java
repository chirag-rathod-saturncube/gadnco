package com.saturncube.gadandco.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.saturncube.gadandco.R;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.DialogManager;
import com.saturncube.gadandco.common.LoadingDialog;
import com.saturncube.gadandco.common.WebServices;
import com.saturncube.gadandco.moduls.Filter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class Getfilters {
    private String response = "";
    private String url = WebServices.GET_FILTERS;
    String TAG = getClass().getSimpleName();

    Context mContext;
    private OnResultReceived onResultReceived;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;

    public Getfilters(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public interface OnResultReceived {
        public void onResult(String result) throws UnsupportedEncodingException;
    }


    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }


    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("success")) {


                                if (response.getString("success").equals("0")) {
                                    hide();
                                    DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                } else if (response.getString("success").equals("1")) {
                                    JSONArray cut = response.getJSONArray("cut");
                                    AppConstants.cutList.clear();
                                    if (cut.length() > 0) {


                                      AppConstants.cutList.add(new Filter("0","-- Select Cut --","cut"));

                                        for (int i = 0; i < cut.length(); i++) {
                                            JSONObject object = cut.getJSONObject(i);
                                            Filter filter = new Filter();
                                            filter.setId(object.optString("cut_id"));
                                            filter.setName(object.optString("cut_name"));
                                            filter.setCode(object.optString("code"));

                                            AppConstants.cutList.add(filter);
                                        }
                                    }



                                    JSONArray color = response.getJSONArray("color");
                                    AppConstants.colorList.clear();
                                    if (color.length() > 0) {
                                        AppConstants.colorList.add(new Filter("0","-- Select Color --","color"));
                                        for (int i = 0; i < color.length(); i++) {
                                            JSONObject object = color.getJSONObject(i);
                                            Filter filter = new Filter();
                                            filter.setId(object.optString("color_id"));
                                            filter.setName(object.optString("color_name"));
                                            filter.setCode(object.optString("code"));

                                            AppConstants.colorList.add(filter);
                                        }
                                    }

                                    JSONArray clarity = response.getJSONArray("clarity");
                                    AppConstants.clarityList.clear();
                                    if (clarity.length() > 0) {
                                        AppConstants.clarityList.add(new Filter("0","-- Select Clarity --","clarity"));
                                        for (int i = 0; i < clarity.length(); i++) {
                                            JSONObject object = clarity.getJSONObject(i);
                                            Filter filter = new Filter();
                                            filter.setId(object.optString("clarity_id"));
                                            filter.setName(object.optString("clarity_name"));
                                            filter.setCode(object.optString("code"));

                                            AppConstants.clarityList.add(filter);
                                        }
                                    }

                                    JSONArray carat = response.getJSONArray("carat");
                                    AppConstants.caratList.clear();
                                    if (carat.length() > 0) {
                                        AppConstants.caratList.add(new Filter("0","-- Select Carat --","carat"));
                                        for (int i = 0; i < carat.length(); i++) {
                                            JSONObject object = carat.getJSONObject(i);
                                            Filter filter = new Filter();
                                            filter.setId(object.optString("carat_id"));
                                            filter.setName(object.optString("carat_name"));
                                            filter.setCode(object.optString("code"));
                                            AppConstants.caratList.add(filter);
                                        }
                                    }

                                    hide();
                                    if (onResultReceived != null) try {
                                        onResultReceived.onResult(response.toString());
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    hide();
                                    Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };
        queue.add(postRequest);
    }
}
