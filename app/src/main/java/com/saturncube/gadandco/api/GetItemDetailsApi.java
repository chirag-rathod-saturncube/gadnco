package com.saturncube.gadandco.api;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.saturncube.gadandco.R;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.DialogManager;
import com.saturncube.gadandco.common.LoadingDialog;
import com.saturncube.gadandco.common.WebServices;
import com.saturncube.gadandco.moduls.Image;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pc4 on 7/6/2016.
 */
public class GetItemDetailsApi {
    String TAG = getClass().getSimpleName();
    Context mContext;
    String envelope = "";
    LoadingDialog loadingDialog;
    RequestQueue queue;
    boolean flagProgress;
    private String response = "";
    private String url = WebServices.GET_ITEM_DETAIL;
    private OnResultReceived onResultReceived;

    public GetItemDetailsApi(String envelope, Context mContext, OnResultReceived onResultReceived, boolean flagProgress) {
        Log.e(TAG, "GetItemDetailsApi(){...}");
        this.envelope = envelope;
        this.mContext = mContext;
        this.onResultReceived = onResultReceived;
        this.flagProgress = flagProgress;
        queue = Volley.newRequestQueue(mContext);
        getResponse();
    }

    public void show() {
        if (flagProgress) {
            loadingDialog = new LoadingDialog(mContext, "", false);
        }
    }

    public void hide() {
        if (flagProgress) {
            loadingDialog.hide();
        }
    }

    public void getResponse() {
        show();
        Log.e(TAG, "Url: " + url + envelope);
        JsonObjectRequest postRequest = new JsonObjectRequest(com.android.volley.Request.Method.GET, url + envelope, null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e(TAG, "Response: " + response);
                            if (response.has("success")) {


                                if (response.getString("success").equals("0")) {
                                    hide();
                                    DialogManager.errorDialog(mContext, "Error!", response.optString("msg"));
                                } else if (response.getString("success").equals("1")) {
                                    JSONObject result = response.getJSONObject("result");

                                    AppConstants.stone = result.optString("stone");
                                    AppConstants.certificate = result.optString("certificate");
                                    AppConstants.certificate_no = result.optString("certificate_no");
                                    AppConstants.item_type = result.optString("item_type");
                                    AppConstants.treatment = result.optString("treatment");
                                    AppConstants.color = result.optString("color");
                                    AppConstants.location = result.optString("location");
                                    AppConstants.cut = result.optString("cut");
                                    AppConstants.clarity = result.optString("clarity");
                                    AppConstants.carat = result.optString("carat");
                                    AppConstants.rapa_port_price = result.optString("rapa_port_price");
                                    AppConstants.item_image = result.optString("item_pic");


                                    if (result.has("retail_price")) {
                                        AppConstants.retail_price = result.optString("retail_price");
                                    } else {
                                        AppConstants.retail_price = "";
                                    }
                                    if (result.has("wholesale_price")) {
                                        AppConstants.wholesale_price = result.optString("wholesale_price");
                                    } else {
                                        AppConstants.wholesale_price = "";
                                    }


                                    if (result.has("images")) {
                                        AppConstants.imageList.clear();
                                        JSONArray images = result.getJSONArray("images");
                                        if (images.length() > 0) {
                                            for (int i = 0; i < images.length(); i++) {
                                                JSONObject object = images.getJSONObject(i);
                                                Image image = new Image();
                                                image.setImage_id(object.optString("image_id"));
                                                image.setItem_id(object.optString("item_id"));
                                                image.setItem_pic(object.optString("item_pic"));
                                                image.setItem_img_small(object.optString("item_img_small"));
                                                image.setItem_img_medium(object.optString("item_img_medium"));
                                                image.setItem_img_large(object.optString("item_img_large"));
                                                AppConstants.imageList.add(image);
                                            }
                                        }
                                    }

                                    hide();
                                    if (onResultReceived != null) try {
                                        onResultReceived.onResult(response.toString());
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                } else {
                                    hide();
                                    Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                                }
                            } else {
                                hide();
                                Toast.makeText(mContext, mContext.getString(R.string.something_was_wrong_with_api), Toast.LENGTH_LONG).show();
                            }

                        } catch (Exception e) {
                            hide();
                            Log.e(TAG, "Line " + Thread.currentThread().getStackTrace()[2].getLineNumber() + " Error:... " + e.getMessage());
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        hide();
                        VolleyLog.d("Error.Response", "");
                    }
                }
        ) {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");
                params.put("Cache-Control", "no-cache");
                return params;
            }
        };
        queue.add(postRequest);
    }


    public interface OnResultReceived {
        public void onResult(String result) throws UnsupportedEncodingException;
    }
}
