package com.saturncube.gadandco;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.saturncube.gadandco.api.GetItemApi;
import com.saturncube.gadandco.api.Getfilters;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.Preferences;
import com.saturncube.gadandco.moduls.Filter;

import java.io.UnsupportedEncodingException;

/*import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;*/
/*import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.saturncube.gadandco.adapter.ItemAdapter;*/

public class Filter_Activity extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    ImageView img_back;
    TextView tv_header_title;


    ImageView img_title, img_option;
    CrystalRangeSeekbar rangeSeekbar;
    TextView tv_minprice, tv_maxprice;

    Spinner sp_cut, sp_color, sp_clarity, sp_carat;
    ArrayAdapter<Filter> cut_adapter, color_adapter, clarity_adapter, carat_adapter;


    String cut, color, clarity, carat, start_price, end_price = "";
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_);
        initView();


        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tv_minprice.setText("$" + String.valueOf(minValue));
                tv_maxprice.setText("$" + String.valueOf(maxValue));

                start_price = String.valueOf(minValue);
                end_price = String.valueOf(maxValue);
            }
        });





        img_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Preferences.setValue(Filter_Activity.this, Preferences.CUT, cut);
                Preferences.setValue(Filter_Activity.this, Preferences.COLOR, color);
                Preferences.setValue(Filter_Activity.this, Preferences.CLARITY, clarity);
                Preferences.setValue(Filter_Activity.this, Preferences.CARAT, carat);
                Preferences.setValue(Filter_Activity.this, Preferences.START_PRICE, start_price);
                Preferences.setValue(Filter_Activity.this, Preferences.END_PRICE, end_price);

                filter();
            }
        });


        new Getfilters("", Filter_Activity.this, new Getfilters.OnResultReceived() {
            @Override
            public void onResult(String result) throws UnsupportedEncodingException {
                if (AppConstants.cutList.size() > 0) {
                    cut_adapter = new ArrayAdapter<Filter>(Filter_Activity.this, R.layout.spinner_item, AppConstants.cutList);
                    sp_cut.setAdapter(cut_adapter);
                }
                if (AppConstants.colorList.size() > 0) {
                    color_adapter = new ArrayAdapter<Filter>(Filter_Activity.this, R.layout.spinner_item, AppConstants.colorList);
                    sp_color.setAdapter(color_adapter);
                }
                if (AppConstants.clarityList.size() > 0) {
                    clarity_adapter = new ArrayAdapter<Filter>(Filter_Activity.this, R.layout.spinner_item, AppConstants.clarityList);
                    sp_clarity.setAdapter(clarity_adapter);
                }
                if (AppConstants.caratList.size() > 0) {
                    carat_adapter = new ArrayAdapter<Filter>(Filter_Activity.this, R.layout.spinner_item, AppConstants.caratList);
                    sp_carat.setAdapter(carat_adapter);
                }


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        setdata();
                    }
                }, 1000);


            }
        }, true);

        sp_cut.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Filter category = (Filter) sp_cut.getSelectedItem();

                if (category.getId().equals("0")) {

                    cut = "";
                } else {
                    cut = category.getName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        sp_color.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Filter category = (Filter) sp_color.getSelectedItem();

                if (category.getId().equals("0")) {

                    color = "";
                } else {
                    color = category.getName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_clarity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Filter category = (Filter) sp_clarity.getSelectedItem();

                if (category.getId().equals("0")) {

                    clarity = "";
                } else {
                    clarity = category.getName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        sp_carat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Filter category = (Filter) sp_carat.getSelectedItem();

                if (category.getId().equals("0")) {

                    carat = "";
                } else {
                    carat = category.getName();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reset();
                filter();
            }
        });


    }

    private void initView() {
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_minprice = (TextView) findViewById(R.id.tv_minprice);
        tv_maxprice = (TextView) findViewById(R.id.tv_maxprice);
        tv_header_title.setText("FILTERS");
        tv_header_title.setVisibility(View.VISIBLE);
        img_back.setVisibility(View.VISIBLE);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });


        img_title = (ImageView) findViewById(R.id.img_title);
        img_title.setVisibility(View.GONE);
        img_option = (ImageView) findViewById(R.id.img_option);
        img_option.setImageResource(R.drawable.ic_done);
        img_option.setVisibility(View.VISIBLE);

        rangeSeekbar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar);

        sp_cut = (Spinner) findViewById(R.id.sp_cut);
        sp_color = (Spinner) findViewById(R.id.sp_color);
        sp_clarity = (Spinner) findViewById(R.id.sp_clarity);
        sp_carat = (Spinner) findViewById(R.id.sp_carat);
        btnLogin = (Button) findViewById(R.id.btnLogin);

    }

    private void filter() {
        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), Filter_Activity.this)) {

            String param = "&cat_id=" + AppConstants.Category_id;
            param = param + "&role_id=" + Preferences.getValue_String(Filter_Activity.this, Preferences.USER_ROLE);
            param = param + "&cut=" + Preferences.getValue_String(Filter_Activity.this, Preferences.CUT);
            param = param + "&color=" + Preferences.getValue_String(Filter_Activity.this, Preferences.COLOR);
            param = param + "&clarity=" + Preferences.getValue_String(Filter_Activity.this, Preferences.CLARITY);
            param = param + "&carat=" + Preferences.getValue_String(Filter_Activity.this, Preferences.CARAT);
            param = param + "&startprice=" + Preferences.getValue_String(Filter_Activity.this, Preferences.START_PRICE);
            param = param + "&endprice=" + Preferences.getValue_String(Filter_Activity.this, Preferences.END_PRICE);
            new GetItemApi(param, Filter_Activity.this, new GetItemApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    finish();
                }
            }, true);

        }
    }

    private int getIndex(Spinner spinner, String myString) {
        int index = 0;
        for (int i = 0; i < spinner.getCount(); i++) {

            if (spinner.getItemAtPosition(i).toString().equals(myString)) {
                index = i;
            }
        }
        return index;
    }

    public void reset() {
        Preferences.setValue(Filter_Activity.this, Preferences.CUT, "");
        Preferences.setValue(Filter_Activity.this, Preferences.COLOR, "");
        Preferences.setValue(Filter_Activity.this, Preferences.CLARITY, "");
        Preferences.setValue(Filter_Activity.this, Preferences.CARAT, "");
        Preferences.setValue(Filter_Activity.this, Preferences.START_PRICE, "");
        Preferences.setValue(Filter_Activity.this, Preferences.END_PRICE, "");
    }

    public void setdata() {
        if (!Preferences.getValue_String(Filter_Activity.this, Preferences.CUT).equals("")) {
            sp_cut.setSelection(getIndex(sp_cut, Preferences.getValue_String(Filter_Activity.this, Preferences.CUT)));
        }
        if (!Preferences.getValue_String(Filter_Activity.this, Preferences.COLOR).equals("")) {
            sp_color.setSelection(getIndex(sp_color, Preferences.getValue_String(Filter_Activity.this, Preferences.COLOR)));
        }

        if (!Preferences.getValue_String(Filter_Activity.this, Preferences.CLARITY).equals("")) {
            sp_clarity.setSelection(getIndex(sp_clarity, Preferences.getValue_String(Filter_Activity.this, Preferences.CLARITY)));
        }

        if (!Preferences.getValue_String(Filter_Activity.this, Preferences.CARAT).equals("")) {
            sp_carat.setSelection(getIndex(sp_carat, Preferences.getValue_String(Filter_Activity.this, Preferences.CARAT)));
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!Preferences.getValue_String(Filter_Activity.this, Preferences.START_PRICE).equals("")) {
                    rangeSeekbar.setMinStartValue(Float.parseFloat(Preferences.getValue_String(Filter_Activity.this, Preferences.START_PRICE))).apply();

                }

                if (!Preferences.getValue_String(Filter_Activity.this, Preferences.END_PRICE).equals("")) {
                    rangeSeekbar.setMaxStartValue(Float.parseFloat(Preferences.getValue_String(Filter_Activity.this, Preferences.END_PRICE))).apply();
                }
            }
        }, 1000);




       rangeSeekbar.setMinValue(50000f);
    }
}
