package com.saturncube.gadandco;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saturncube.gadandco.adapter.CategoryAdapter;
import com.saturncube.gadandco.api.GetCategoryApi;
import com.saturncube.gadandco.common.AppConstants;

import java.io.UnsupportedEncodingException;

public class MainActivity extends AppCompatActivity {
    String TAG = getClass().getSimpleName();
    ImageView img_back;
    TextView tv_header_title;

    RelativeLayout rel_no_record;
    RecyclerView recycler_view;
    CategoryAdapter adapter;
    GridLayoutManager mLayoutManager;

    ImageView img_title, img_option;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), MainActivity.this)) {
            new GetCategoryApi("", MainActivity.this, new GetCategoryApi.OnResultReceived() {
                @Override
                public void onResult(String result) throws UnsupportedEncodingException {
                    if (AppConstants.categoryList.size() > 0) {
                        rel_no_record.setVisibility(View.GONE);
                        adapter = new CategoryAdapter(MainActivity.this, AppConstants.categoryList);
                        recycler_view.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    } else {
                        rel_no_record.setVisibility(View.VISIBLE);
                    }
                }
            }, true);

        }


        img_option.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingsActivity.class));
            }
        });

    }

    private void initView() {
        img_back = (ImageView) findViewById(R.id.img_back);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        //tv_header_title.setText(getString(R.string.text_lbl_act_demo_activity));
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        rel_no_record = (RelativeLayout) findViewById(R.id.rel_no_record);
        recycler_view = (RecyclerView) findViewById(R.id.recycler_view);
        mLayoutManager = new GridLayoutManager(MainActivity.this, 2);
        boolean includeEdge = true;
        int spacing = 2;
        //recycler_view.addItemDecoration(new GridSpacingItemDecoration(2, spacing, includeEdge));

        mLayoutManager.setSmoothScrollbarEnabled(true);
        recycler_view.setLayoutManager(mLayoutManager);


        img_title = (ImageView) findViewById(R.id.img_title);
        img_title.setVisibility(View.VISIBLE);
        img_option = (ImageView) findViewById(R.id.img_option);
        img_option.setVisibility(View.VISIBLE);

    }

}
