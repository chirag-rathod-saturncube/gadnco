package com.saturncube.gadandco;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.DialogManager;

public class RegisterActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();
    EditText edt_first_name, edt_last_name, edt_email, edt_password;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        register();
                    }
                }, 300);
            }
        });

        edt_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    register();
                }
                return false;
            }
        });

    }

    private void initView() {
        edt_first_name = (EditText) findViewById(R.id.edt_first_name);
        edt_last_name = (EditText) findViewById(R.id.edt_last_name);
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        btnRegister = (Button) findViewById(R.id.btnRegister);
    }

    public void register() {
        if (isValidate()) {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), RegisterActivity.this)) {
               /* try {
                    String envelope = "&email=" + URLEncoder.encode(edt_email.getText().toString().trim(), AppConstants.encodeType);
                    envelope = envelope + "&password=" + URLEncoder.encode(edt_password.getText().toString().trim(), AppConstants.encodeType);
                    envelope = envelope + "&connectTo=" + AppConstants.CAM8844;
                    envelope = envelope + "&gcm_id=" + FirebaseInstanceId.getInstance().getToken();
                    envelope = envelope + "&device_id=" + Settings.Secure.getString(LoginActivity.this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    envelope = envelope + "&type=1";

                    *//*new Login(envelope, LoginActivity.this, new Login.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {

                        }
                    }).execute();*//*


                    new LoginApi(envelope, LoginActivity.this, new LoginApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) throws UnsupportedEncodingException {
                            Preferences.setValue(LoginActivity.this, Preferences.UserEmail, edt_email.getText().toString().trim());
                            Preferences.setValue(LoginActivity.this, Preferences.Password, edt_password.getText().toString().trim());
                            Preferences.setValue(LoginActivity.this, Preferences.PersonConnectedTo, AppConstants.CAM8844);
                            Preferences.setValue(LoginActivity.this, Preferences.PersonRelevantProfile, "");
                            Preferences.setValue(LoginActivity.this, Preferences.PersonRelevantID, "");
                            Intent home = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(home);
                            finish();
                        }
                    }, true);

                } catch (UnsupportedEncodingException e) {
                    Log.e(TAG, "UnsupportedEncodingException: " + e.getMessage());
                }*/
            }
        }
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";

        if (edt_first_name.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_first_name);
            flagValidate = false;
        }

        if (edt_last_name.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_last_name);
            flagValidate = false;
        }

        if (edt_email.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        }

        if (!AppConstants.validateEmail(edt_email.getText().toString().trim())) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        }

        if (edt_password.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(RegisterActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

}

