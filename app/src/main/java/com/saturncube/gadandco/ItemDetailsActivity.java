package com.saturncube.gadandco;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saturncube.gadandco.adapter.SlideShowAdapter;
import com.saturncube.gadandco.appconfig.Application;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.Preferences;

public class ItemDetailsActivity extends AppCompatActivity {
    private final String TAG = getClass().getSimpleName();
    TextView tv_item, tv_location, tv_cut, tv_stone, tv_clarity, tv_certificate, tv_carat, tv_treatment, tv_certificate_no, tv_Item_Type_Name, tv_rapa_port, tv_color, tv_price, lbl_price, tv_header_title;
    ImageView img_back, img_right, img_left;
    ViewPager view_pager;
    SlideShowAdapter adapter;
    RelativeLayout rel_viewpager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);
        initView();
        setScreendata();
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (AppConstants.imageList.size() > 0) {
            view_pager.setVisibility(View.VISIBLE);
            rel_viewpager.setVisibility(View.VISIBLE);
            adapter = new SlideShowAdapter(ItemDetailsActivity.this, AppConstants.imageList);
            view_pager.setAdapter(adapter);
        } else {
            view_pager.setVisibility(View.GONE);
            rel_viewpager.setVisibility(View.GONE);
        }

        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                if (position == 0) {
                    img_left.setVisibility(View.GONE);
                } else {
                    img_left.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        img_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_pager.setCurrentItem(view_pager.getCurrentItem() - 1);


            }
        });
        img_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                view_pager.setCurrentItem(view_pager.getCurrentItem() + 1);
            }
        });


    }

    private void initView() {

        tv_item = (TextView) findViewById(R.id.tv_item);
        tv_cut = (TextView) findViewById(R.id.tv_cut);
        tv_stone = (TextView) findViewById(R.id.tv_stone);
        tv_clarity = (TextView) findViewById(R.id.tv_clarity);
        tv_certificate = (TextView) findViewById(R.id.tv_certificate);
        tv_carat = (TextView) findViewById(R.id.tv_carat);
        tv_certificate_no = (TextView) findViewById(R.id.tv_certificate_no);
        tv_Item_Type_Name = (TextView) findViewById(R.id.tv_Item_Type_Name);
        tv_treatment = (TextView) findViewById(R.id.tv_treatment);
        tv_rapa_port = (TextView) findViewById(R.id.tv_rapa_port);
        tv_color = (TextView) findViewById(R.id.tv_color);
        tv_location = (TextView) findViewById(R.id.tv_location);
        tv_price = (TextView) findViewById(R.id.tv_price);
        lbl_price = (TextView) findViewById(R.id.lbl_price);
        tv_header_title = (TextView) findViewById(R.id.tv_header_title);
        tv_header_title.setVisibility(View.VISIBLE);
        tv_header_title.setText(AppConstants.item_name.toUpperCase());
        img_back = (ImageView) findViewById(R.id.img_back);
        img_right = (ImageView) findViewById(R.id.img_right);
        img_left = (ImageView) findViewById(R.id.img_left);
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        rel_viewpager = (RelativeLayout) findViewById(R.id.rel_viewpager);
        img_back.setVisibility(View.VISIBLE);
    }

    private void setScreendata() {
        tv_item.setText(AppConstants.item_id);
        tv_cut.setText(AppConstants.cut);
        tv_stone.setText(AppConstants.stone);
        tv_clarity.setText(AppConstants.clarity);
        tv_certificate.setText(AppConstants.certificate);
        tv_carat.setText(AppConstants.carat);
        tv_certificate_no.setText(AppConstants.certificate_no);
        tv_Item_Type_Name.setText(AppConstants.item_type);
        tv_treatment.setText(AppConstants.treatment);
        tv_rapa_port.setText(AppConstants.rapa_port_price);
        tv_color.setText(AppConstants.color);
        tv_location.setText(AppConstants.location);


        if (Preferences.getValue_String(ItemDetailsActivity.this, Preferences.USER_ROLE).equals("1")) {
            lbl_price.setText("Retail Price");
            tv_price.setText(AppConstants.retail_price);
        } else {
            lbl_price.setText("Price");
            tv_price.setText(AppConstants.wholesale_price);
        }
    }
}
