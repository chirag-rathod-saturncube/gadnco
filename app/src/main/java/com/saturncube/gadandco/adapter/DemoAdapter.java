package com.saturncube.gadandco.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.saturncube.gadandco.R;
import com.saturncube.gadandco.moduls.Demo;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by pc4 on 5/30/2016.
 */
public class DemoAdapter extends RecyclerView.Adapter<DemoAdapter.CustomViewHolder> {
    private ArrayList<Demo> arrayList;
    private Context mContext;
    List<Demo> infos = null;
    private String TAG = getClass().getSimpleName();


    public DemoAdapter(Context context, List<Demo> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category_layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Demo list = infos.get(pos);



    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        infos.clear();
        for (Demo wp : arrayList) {
            /*if (wp.getUsername().toLowerCase(Locale.getDefault()).contains(charText)) {
                infos.add(wp);
                Log.e(TAG, "Info search: " + wp.getUsername());
            }*/
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {



        public CustomViewHolder(View view) {
            super(view);


        }
    }
}

