package com.saturncube.gadandco.adapter;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DownloadManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.saturncube.gadandco.R;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.moduls.Image;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;


/**
 * Created by pc4 on 10/16/2015.
 */
public class SlideShowAdapter extends PagerAdapter {
    private static final int REQUEST_PERMISSION_CODE = 10000;
    Context context;
    String TAG = getClass().getSimpleName();
    ArrayList<Image> arrayList;
    BroadcastReceiver onComplete = new BroadcastReceiver() {
        public void onReceive(Context ctxt, Intent intent) {
            Log.e(TAG, "onComplete(){...}");
            Toast.makeText(context, "Download Done", Toast.LENGTH_SHORT).show();
        }
    };

    public SlideShowAdapter(Context context, ArrayList<Image> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, final int position) {
        final Image list = arrayList.get(position);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.slide_item, container, false);
        final ImageView imageView = (ImageView) viewLayout.findViewById(R.id.img_slide);
        Log.e(TAG, "Slide: " + list.getItem_img_large());

        imageView.setPadding(4, 4, 4, 4);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        Picasso.with(context)
                .load(list.getItem_img_large())
                .placeholder(R.drawable.gadnco)
                /*.transform(AppConstants.getTransformation(imageView))*/
                .into(imageView);

        imageView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, "Start Downloading...", Toast.LENGTH_SHORT).show();
                checkPermission(list.getItem_img_large());
                return true;
            }


        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onClick(View v) {
                //String url = AppConstants.imageList.get(position).getItem_image();
                Log.e(TAG, "Url : " + list.getItem_img_large());
                final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context, R.style.DialogFullScreen);
                final LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.dialog_fullscreen, null);
                dialogBuilder.setView(dialogView);
                final AlertDialog b = dialogBuilder.create();
                b.show();

                ImageView imageview = (ImageView) dialogView.findViewById(R.id.imageviewfullscreen);
                ImageView imgBack = (ImageView) dialogView.findViewById(R.id.imgBack);
                TextView tvHeader = (TextView) dialogView.findViewById(R.id.tvHeader);
                tvHeader.setText(AppConstants.item_name.toUpperCase());
                imgBack.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        b.dismiss();
                    }
                });
                Picasso.with(context)
                        .load(list.getItem_img_large())
                        /*.transform(AppConstants.getTransformation(imageview))*/
                        .placeholder(R.drawable.gadnco)
                        .into(imageview);
                imageview.setOnTouchListener(new ImageMatrixTouchHandler(context));
            }
        });
        ((ViewPager) container).addView(viewLayout, 0);
        return viewLayout;
    }

    private void checkPermission(String imageUrl) {
        if (Build.VERSION.SDK_INT >= 23) {
            if (((Activity) context).checkSelfPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ((Activity) context).checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                internalStorageReadWritePermission();
            } else {
                downloadImageFile(imageUrl);
            }
        } else {
            downloadImageFile(imageUrl);
        }
    }

    private void internalStorageReadWritePermission() {
        String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= 23) {
            if (isPermissionGranted(perms)) {
                ((Activity) context).requestPermissions(perms, REQUEST_PERMISSION_CODE);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean isPermissionGranted(String[] perms) {
        for (String perm : perms)
            if (context.checkSelfPermission(perm) != PackageManager.PERMISSION_GRANTED)
                return true;
        return false;
    }

    public void downloadImageFile(final String downloadUrl) {
        Log.e(TAG, "downloadFile(){...}");
        String fileName = downloadUrl.substring(downloadUrl.lastIndexOf('/') + 1);
        Log.e(TAG, "fileName : " + fileName);
        File direct = new File(Environment.DIRECTORY_DOWNLOADS + "/" + context.getString(R.string.app_name) + "/");
        if (!direct.exists()) {
            direct.mkdir();
            Log.e(TAG, "dir created for first time");
        }
        File file = new File(direct, fileName);
        if (!file.exists()) {
            DownloadManager dm = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(downloadUrl);
            DownloadManager.Request request = new DownloadManager.Request(downloadUri);
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                    .setAllowedOverRoaming(false)
                    .setTitle(fileName)
                    .setMimeType("image/*")
                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                            File.separator + context.getString(R.string.app_name) + File.separator + fileName)
                    .allowScanningByMediaScanner();

            dm.enqueue(request);
            context.registerReceiver(onComplete, new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        } else {
            Toast.makeText(context, "Image downloaded, please check from gallery.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);
    }
}