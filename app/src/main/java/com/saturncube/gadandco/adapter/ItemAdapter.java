package com.saturncube.gadandco.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saturncube.gadandco.ItemDetailsActivity;
import com.saturncube.gadandco.R;
import com.saturncube.gadandco.api.GetItemDetailsApi;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.Preferences;
import com.saturncube.gadandco.moduls.Item;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by pc4 on 5/30/2016.
 */
public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.CustomViewHolder> {
    List<Item> infos = null;
    private ArrayList<Item> arrayList;
    private Context mContext;
    private String TAG = getClass().getSimpleName();


    public ItemAdapter(Context context, List<Item> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Item list = infos.get(pos);
        customViewHolder.tv_name.setText(list.getStone().toUpperCase());
        customViewHolder.tv_certificate.setText("Certificate:" + list.getCertificate());
        customViewHolder.tv_certificate_no.setText("Certificate Number:" + list.getCertificate_no());

        if (!list.getItem_img_small().equals("")) {
            Picasso.with(mContext)
                    .load(list.getItem_img_small())
                    //.transform(AppConstants.getTransformation(customViewHolder.item_img_small))
                    //.placeholder(R.drawable.ic_gadnco)
                    .into(customViewHolder.item_img_small, new Callback() {
                        @Override
                        public void onSuccess() {
                            Picasso.with(mContext)
                                    .load(list.getItem_img_medium())
                                    //.transform(AppConstants.getTransformation(customViewHolder.item_img_small))
                                    //.placeholder(R.drawable.ic_gadnco)
                                    .into(customViewHolder.item_img_medium, new Callback() {
                                        @Override
                                        public void onSuccess() {
                                            Picasso.with(mContext)
                                                    .load(list.getItem_img_large())
                                                    //.transform(AppConstants.getTransformation(customViewHolder.item_img_small))
                                                    //.placeholder(R.drawable.ic_gadnco)
                                                    .into(customViewHolder.item_img_large, new Callback() {
                                                        @Override
                                                        public void onSuccess() {

                                                        }

                                                        @Override
                                                        public void onError() {

                                                        }
                                                    });

                                        }

                                        @Override
                                        public void onError() {

                                        }
                                    });

                        }

                        @Override
                        public void onError() {

                        }
                    });
        }


        customViewHolder.rel_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e(TAG, "OnClick(){...}");
                if (AppConstants.isNetworkAvailable(mContext.getString(R.string.network_title), mContext.getString(R.string.network_msg), mContext)) {
                    String param = "&cat_id=" + AppConstants.Category_id;
                    param = param + "&item_id=" + list.getItem_id();
                    param = param + "&role_id=" + Preferences.getValue_String(mContext, Preferences.USER_ROLE);

                    new GetItemDetailsApi(param, mContext, new GetItemDetailsApi.OnResultReceived() {
                        @Override
                        public void onResult(String result) {
                            Log.e(TAG, "onResult(){...}");
                            AppConstants.item_id = list.getItem_id();
                            AppConstants.item_name = list.getStone();
                            mContext.startActivity(new Intent(mContext, ItemDetailsActivity.class));
                        }
                    }, true);


                }
            }
        });

    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        infos.clear();
        for (Item wp : arrayList) {
            if (wp.getStone().toLowerCase(Locale.getDefault()).contains(charText)) {
                infos.add(wp);
                Log.e(TAG, "Info search: " + wp.getStone());
            }
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {

        TextView tv_name, tv_certificate, tv_certificate_no;

        RelativeLayout rel_main;
        ImageView item_img_small, item_img_medium, item_img_large;

        public CustomViewHolder(View view) {
            super(view);

            this.tv_name = (TextView) view.findViewById(R.id.tv_name);
            this.tv_certificate = (TextView) view.findViewById(R.id.tv_certificate);
            this.tv_certificate_no = (TextView) view.findViewById(R.id.tv_certificate_no);
            this.item_img_small = (ImageView) view.findViewById(R.id.item_img_small);
            this.item_img_medium = (ImageView) view.findViewById(R.id.item_img_medium);
            this.item_img_large = (ImageView) view.findViewById(R.id.item_img_large);
            rel_main = (RelativeLayout) view.findViewById(R.id.rel_main);
        }
    }
}

