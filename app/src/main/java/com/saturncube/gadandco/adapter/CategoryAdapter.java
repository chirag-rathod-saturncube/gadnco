package com.saturncube.gadandco.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.saturncube.gadandco.ItemActivity;
import com.saturncube.gadandco.R;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.moduls.Category;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


/**
 * Created by pc4 on 5/30/2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CustomViewHolder> {
    private ArrayList<Category> arrayList;
    private Context mContext;
    List<Category> infos = null;
    private String TAG = getClass().getSimpleName();


    public CategoryAdapter(Context context, List<Category> list) {
        this.infos = list;
        this.arrayList = new ArrayList<>();
        this.arrayList.addAll(infos);
        this.mContext = context;
    }


    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_category_layout, null);
        CustomViewHolder viewHolder = new CustomViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder customViewHolder, final int pos) {
        final Category list = infos.get(pos);

        if (!list.getCategory_image().equals("")) {
            Picasso.with(mContext)
                    .load(list.getCategory_image())
                    .transform(AppConstants.getTransformation(customViewHolder.img_cat))
                    .into(customViewHolder.img_cat);
        }


        customViewHolder.tv_catname.setText(list.getCategory_name().toUpperCase());

        customViewHolder.card_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.Category_name = list.getCategory_name();
                AppConstants.Category_id = list.getCategory_id();
                mContext.startActivity(new Intent(mContext, ItemActivity.class));
            }
        });


    }


    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        infos.clear();
        for (Category wp : arrayList) {
            /*if (wp.getUsername().toLowerCase(Locale.getDefault()).contains(charText)) {
                infos.add(wp);
                Log.e(TAG, "Info search: " + wp.getUsername());
            }*/
        }
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return (null != infos ? infos.size() : 0);
    }


    public class CustomViewHolder extends RecyclerView.ViewHolder {
        CardView card_view;
        ImageView img_cat;
        TextView tv_catname;


        public CustomViewHolder(View view) {
            super(view);
            this.img_cat = (ImageView) view.findViewById(R.id.img_cat);

            this.tv_catname = (TextView) view.findViewById(R.id.tv_catname);
            card_view = (CardView) view.findViewById(R.id.card_view);
        }
    }
}

