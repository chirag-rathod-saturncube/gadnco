package com.saturncube.gadandco;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.saturncube.gadandco.api.ForgotPasswordApi;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.DialogManager;

import java.io.UnsupportedEncodingException;

public class ForgotPasswordActivity extends AppCompatActivity {

    private String TAG = getClass().getSimpleName();
    EditText edt_email;
    Button btnSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initView();
        edt_email.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    send();
                }
                return false;
            }
        });

        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        send();
                    }
                }, 300);
            }
        });


    }

    private void initView() {
        edt_email = (EditText) findViewById(R.id.edt_email);
        btnSend = (Button) findViewById(R.id.btnSend);
    }

    private void send() {
        if (isValidate()) {
            //Call api here:
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), ForgotPasswordActivity.this)) {


                String param = "&email=" + edt_email.getText().toString();
                new ForgotPasswordApi(param, ForgotPasswordActivity.this, new ForgotPasswordApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {

                    }
                }, true);

            }

        }
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";
        if (edt_email.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        }

        if (!AppConstants.validateEmail(edt_email.getText().toString())) {
            validateMsg = getString(R.string.text_validate_valid_email);
            flagValidate = false;
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(ForgotPasswordActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }

    }

}
