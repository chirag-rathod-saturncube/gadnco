package com.saturncube.gadandco;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.saturncube.gadandco.api.LoginApi;
import com.saturncube.gadandco.common.AppConstants;
import com.saturncube.gadandco.common.DialogManager;

import java.io.UnsupportedEncodingException;

public class LoginActivity extends AppCompatActivity {
    private String TAG = getClass().getSimpleName();
    public EditText edt_email, edt_password;
    Button btnLogin;
    ImageView img_facebook, img_google;
    RelativeLayout rel_forgot_pass, rel_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();

        edt_password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    login();
                }
                return false;
            }
        });
        edt_password.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                if (keyCode == KeyEvent.KEYCODE_DEL) {
                    //this is for backspace
                    edt_password.setText("");
                }
                return false;
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        login();
                    }
                }, 300);
            }
        });


        img_facebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        img_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        rel_forgot_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                        startActivity(intent);
                    }
                }, 300);
            }
        });

        rel_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                        startActivity(intent);
                    }
                }, 300);
            }
        });


    }

    private void initView() {
        edt_email = (EditText) findViewById(R.id.edt_email);
        edt_password = (EditText) findViewById(R.id.edt_password);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        img_facebook = (ImageView) findViewById(R.id.img_facebook);
        img_google = (ImageView) findViewById(R.id.img_google);
        rel_forgot_pass = (RelativeLayout) findViewById(R.id.rel_forgot_pass);
        rel_register = (RelativeLayout) findViewById(R.id.rel_register);
    }

    private void login() {
        if (isValidate()) {
            if (AppConstants.isNetworkAvailable(getString(R.string.network_title), getString(R.string.network_msg), LoginActivity.this)) {


                String envelope = "&email=" + edt_email.getText().toString();
                envelope = envelope + "&password=" + edt_password.getText().toString();

                new LoginApi(envelope, LoginActivity.this, new LoginApi.OnResultReceived() {
                    @Override
                    public void onResult(String result) throws UnsupportedEncodingException {

                        Intent home = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(home);
                        finish();
                    }

                }, true);


            }
        }
    }

    private boolean isValidate() {
        boolean flagValidate = true;
        String validateMsg = "";

        if (edt_email.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_email);
            flagValidate = false;
        } else if (!AppConstants.validateEmail(edt_email.getText().toString().trim())) {
            validateMsg = getString(R.string.text_validate_valid_email);
            flagValidate = false;
        } else if (edt_password.getText().toString().trim().equals("")) {
            validateMsg = getString(R.string.text_validate_password);
            flagValidate = false;
        }

        if (flagValidate) {
            return true;
        } else {
            DialogManager.errorDialog(LoginActivity.this, getString(R.string.validation_title), validateMsg);
            return false;
        }
    }

    public void clear() {
        edt_email.setText("");
        edt_password.setText("");
    }

}
